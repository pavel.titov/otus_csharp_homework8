﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Homework8
{
    class Program
    {
        static void Main(string[] args)
        {
            var uri = new Uri("https://otus.ru/");
            string downloadFolder = @"C:\Temp\images";

            DownloadImages(uri, downloadFolder);

            Console.WriteLine("Downloading finished, press any key to exit");
            Console.ReadKey();
        }

        static void DownloadImages(Uri uri, string downloadFolder)
        {

            var downloadTasks = new List<Task>();

            using (WebClient client = new WebClient())
            {
                string pageHtml = client.DownloadString(uri);

                var imgRegex = new Regex("<img.*src=\"(.*?)\".*?>");
                var imgMatches = imgRegex.Matches(pageHtml);

                var imgUrls = imgMatches.Cast<Match>()
                    .Select(m => m.Groups[1].Value)
                    .Distinct();

                foreach (string imgUrl in imgUrls)
                {
                    var fullUri = new Uri(uri, imgUrl);

                    var downloadTask = Task.Run(async () =>
                    {
                        Console.WriteLine("Starting to download " + fullUri.AbsoluteUri);
                        await DownloadFile(fullUri, downloadFolder);
                        Console.WriteLine($"Finished downloading {fullUri}");
                    });

                    downloadTasks.Add(downloadTask);
                }
            }

            try
            {
                Task.WaitAll(downloadTasks.ToArray());
            }
            catch (AggregateException ae)
            {
                foreach (Exception e in ae.InnerExceptions)
                {
                    if (e is HttpRequestException || e is IOException)
                    {
                        Console.WriteLine(e.Message);
                    }
                    else
                    {
                        throw e;
                    }
                }
            }
        }

        static async Task DownloadFile(Uri uri, string downloadFolder)
        {
            using HttpClient client = new HttpClient();
            using HttpResponseMessage response = await client.GetAsync(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Failed to download {uri}, status code {(int)response.StatusCode}");
            }

            string filePath = GetFilePathFromResponse(downloadFolder, uri, response);

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            try
            {
                using var fs = new FileStream(filePath, FileMode.CreateNew);
                await response.Content.CopyToAsync(fs);
            }
            catch (IOException)
            {
                throw new IOException($"Failed to save file {filePath}, already exists");
            }
        }

        static string GetFilePathFromResponse(string basePath, Uri requestUri, HttpResponseMessage response)
        {
            string fileName = Path.GetFileName(requestUri.AbsolutePath);

            if (fileName == "" || Path.GetExtension(fileName) == "")
            {
                string fileNameRaw = requestUri.Segments.Last().Trim('/');
                fileName = ReplaceInvalidFileNameChars(fileNameRaw);

                if (Path.GetExtension(fileName) == "")
                {
                    string mediaType = response.Content.Headers.ContentType.MediaType;
                    string extension = mediaType.Split('/').Last();
                    fileName += "." + extension;
                }
            }

            string folderStructure = Path.Combine(GetFolderSegments(requestUri));

            return Path.Combine(basePath, requestUri.Host, folderStructure, fileName);
        }

        static string ReplaceInvalidFileNameChars(string fileName, char replacementChar = '_')
        {
            return string.Join(replacementChar, fileName.Split(Path.GetInvalidFileNameChars()));
        }

        static string[] GetFolderSegments(Uri uri)
        {
            return uri.Segments.SkipLast(1).Select(s => s.Trim('/')).ToArray();
        }
    }
}
